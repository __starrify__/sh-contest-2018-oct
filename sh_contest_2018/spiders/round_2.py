# coding: utf8
'''
WARNING: Coded merely for a contest and potentially BAD practices ahead.

(Commments below are added AFTER the contest.)

Unfortunately I don't have snapshots for all the submissions saved. Here's a
brief description of the stages:
    1. Initial implement. Outcome: missing items.
    2. Attempted enhancing the list travesal approach. Outcome: got
    improvements but was still missing items.
    3. Added the `allowed_domains` attribute and attempted to visit every href
    found (apparantly not good practice :sweat-smile:). Outcome: no more
    missing items, but still incorrect fields.
    4. Performed self-QA and got enhancements on `image_id`. Outcome: there
    were improvements.
    5. Performed self-QA and got enhancements on `flavor`. Outcome: (finally)
    passed.
'''

import json

import scrapy

from sh_contest_2018.utils import DefaultItemLoader


class Round2(scrapy.Spider):
    name = 'round_2'
    start_urls = ['http://flowerpower.scraping.io:8081/listing']
    allowed_domains = ['flowerpower.scraping.io']
    custom_settings = {
        'CONCURRENT_REQUESTS': 64,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 64,
    }

    def parse(self, response):
        if '/listing/i/' in response.url:
            yield from self.parse_image(response)
        if False:
            #for a_sel in response.css('a[href*="listing/i/"]'):
            for a_sel in response.css('div.item a'):
                if a_sel.css('a[href*="listing?page"]'):
                    continue
                yield response.follow(
                    a_sel,
                    callback=self.parse_image
                )
        for a_sel in response.css('a'):
            yield response.follow(
                a_sel,
                callback=self.parse
            )

    def parse_image(self, response):
        item = {
            'item_id': response.css('span#uuid::text').extract_first(),
            'name': response.css('div.left-content h2::text').extract_first(),
            'image_id': (
                response.css('script::text').re_first("const iid = '([^']+)'")
                or response.css('div.right-image img::attr(src)').re_first('/gen/(.*)\.jpg')
            ),
            'flavor': response.xpath('//p[contains(text(),"Flavor: ")]/span/text()').extract_first(),
        }
        if 'thumb' in (item.get('image_id') or ''):
            item['image_id'] = None
        data_flavor = response.css('span.flavor::attr(data-flavor)').extract_first()
        if data_flavor:
            yield response.follow(
                data_flavor,
                meta={
                    'item': item,
                },
                callback=self.parse_flavor
            )
            self.crawler.stats.inc_value('xxx/flavor')
        else:
            yield item

    def parse_flavor(self, response):
        jdata = json.loads(response.text)
        item = response.meta['item']
        item['flavor'] = jdata['value']
        yield item

# coding: utf8

import scrapy


class RoundPractice(scrapy.Spider):
    name = 'round_practice'
    start_urls = ['http://books.toscrape.com/']
    custom_settings = {
        'CONCURRENT_REQUESTS': 64,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 64,
    }

    def parse(self, response):
        num_results = response.css('div.page-header ~ form strong::text').re_first(r'\d+')
        self.logger.debug('Number of results from %s: %s', response, num_results)
        # The number of 1k looks fishy. May be truncated.
        # In case there're more results in subcategories
        for a_sel in response.css('div.side_categories a'):
            yield response.follow(
                a_sel,
                callback=self.parse
            )
        if getattr(self, 'debug_category_results', None) == 'true':
            yield {
                'url': response.url,
                'num_results': num_results,
            }
            return
        # A wast of requests not to filter the products here. But, well.. whatever
        for a_sel in response.css('section article.product_pod h3 a'):
            yield response.follow(
                a_sel,
                callback=self.parse_book
            )
        for a_sel in response.css('ul.pager li.next a'):
            yield response.follow(
                a_sel,
                callback=self.parse
            )

    def parse_book(self, response):
        item = {
            'item_id': response.url,
            'title': response.css('div.product_main h1::text').extract_first(),
            'price': response.css('div.product_main p.price_color::text').extract_first(),
            'category': response.css('ul.breadcrumb li a::text').extract()[-1],
            'image_url': response.urljoin(response.css('div#product_gallery img::attr(src)').extract_first()),
        }
        if float(item['price'][1:]) >= 30:
            self.crawler.stats.inc_value('item_dropped/price_30')
            return
        yield item

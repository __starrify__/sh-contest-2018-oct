# coding: utf8


from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, Identity


class DefaultItemLoader(ItemLoader):
    default_input_processor = lambda loader, values: [str(x).strip() for x in values if x]
    default_output_processor = TakeFirst()



def load_value(src, mapping):
    value = src
    for key in mapping:
        if callable(key):
            value = key(value)
        elif value is None:
            return None
        elif isinstance(key, int) and isinstance(value, list):
            value = value[key]
        elif isinstance(key, str) and isinstance(value, dict):
            value = value[key]
        elif isinstance(key, str) and isinstance(value, list):
            value = [x[key] for x in value]
        elif isinstance(key, (list, tuple)) and isinstance(value, dict):
            value = map_values(value, {}, key)
        elif isinstance(key, (list, tuple)) and isinstance(value, list):
            value = [map_values(x, {}, key) for x in value]
        else:
            raise NotImplementedError
    return value

def map_values(src, dst, mapping, filter_values=(None, [], {})):
    for keys in mapping:
        try:
            value = load_value(src, keys[1:])
        except (ValueError, KeyError, IndexError, TypeError):
            pass
        else:
            if value not in filter_values:
                dst[keys[0]] = value
    return dst
